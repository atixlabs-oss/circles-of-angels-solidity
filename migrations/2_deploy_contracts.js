/**
 * AGPL License
 * Circle of Angels aims to democratize social impact financing.
 * It facilitate the investment process by utilizing smart contracts to develop impact milestones agreed upon by funders and the social entrepenuers.
 *
 * Copyright (C) 2019 AtixLabs, S.R.L <https://www.atixlabs.com>
 */

// const config = require('./deploy_configs.js');

const COAProjectAdmin = artifacts.require('COAProjectAdmin');
const COAOracle = artifacts.require('COAOracle');
const owner = '0xdf08f82de32b8d460adbe8d72043e3a7e25a3b39';

module.exports = async (deployer) => {
  await deployer.deploy(COAOracle);
  const oracleContract = await COAOracle.deployed();
  // console.log(oracleContract);

  const deployed = await deployer.deploy(COAProjectAdmin, owner, oracleContract.address);
  // console.log(deployed);
  // needs to set the whitelisted addresses

  const projectAdmin = await COAProjectAdmin.deployed();
  // await projectAdmin.addWhitelisted(owner);
  console.log('Linking ADMIN SC address on ORACLE SC');
  const response = await oracleContract.setAdminSCAddress(projectAdmin.address); 
  console.log(`Link succesfully on tx : ${response.tx}`);
  console.log('Step 2 finished succesfully');
};
