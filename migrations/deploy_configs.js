/**
 * AGPL License
 * Circle of Angels aims to democratize social impact financing.
 * It facilitate the investment process by utilizing smart contracts to develop impact milestones agreed upon by funders and the social entrepenuers.
 *
 * Copyright (C) 2019 AtixLabs, S.R.L <https://www.atixlabs.com>
 */

const moment = require('moment');
const BigNumber = require('bignumber.js');

const TWO_MONTHS_IN_SECONDS = 2 * 30 * 24 * 60 * 60;

module.exports = {
  walletAddress: '0x0', // CHANGE THIS. Wallet where the funds will be redirected to
  openingTime: moment().utc().unix() + 400, // Change this. Unix timestamp in UTC
  closingTime: moment().utc().unix() + TWO_MONTHS_IN_SECONDS, // Change this. Unix timestamp in UTC

  // Change this. Max amount to raise including presale and crowdsale times usdPrecision
  fundingCapInUsd: (new BigNumber(1000000)).times(1e18).toFixed(),

  // Change this. Amount raised in the presale
  raisedInPresaleInUsd: (new BigNumber(100000)).times(1e18).toFixed(),
  companyPercentage: 48, // Percentage of tokens the company should have at the end

  // Minimum amount of dollars * usdPrecision to apply the discount of that row
  discountThresholds: [
    (new BigNumber(30000).times(1e18)).toFixed(),
    (new BigNumber(50000).times(1e18)).toFixed(),
    (new BigNumber(100000).times(1e18)).toFixed(),
    (new BigNumber(150000).times(1e18)).toFixed(),
    (new BigNumber(200000).times(1e18)).toFixed(),
    (new BigNumber(300000).times(1e18)).toFixed(),
    (new BigNumber(5000000).times(1e18)).toFixed(),
  ],
  // Discount percentage or fixed price
  discountValues: [10, 17, 33, 50, 67, 71, (new BigNumber(0.2)).times(1e18)],
  monthsToStartVestingDiscount: [3, 3, 3, 3, 3, 3, 3],
  monthsToEndVestingDiscount: [6, 6, 6, 6, 6, 6, 6],

  // True for every row where the discount is applied as a percentage,
  // false where the token should be as a fixed price
  discountIsPercentage: [true, true, true, true, true, true, false],
};
