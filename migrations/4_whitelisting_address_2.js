/**
 * AGPL License
 * Circle of Angels aims to democratize social impact financing.
 * It facilitate the investment process by utilizing smart contracts to develop impact milestones agreed upon by funders and the social entrepenuers.
 *
 * Copyright (C) 2019 AtixLabs, S.R.L <https://www.atixlabs.com>
 */

// const config = require('./deploy_configs.js');

const whiteListedAddress = ['0x6704fbfcd5ef766b287262fa2281c105d57246a6'];

const COAProjectAdmin = artifacts.require('COAProjectAdmin');

module.exports = async (deployer) => {
  const projectAdmin = await COAProjectAdmin.deployed();
  console.log(`Whitelisting on project admin contract on address ${projectAdmin.address}`);
  const txAddress = await Promise.all(whiteListedAddress.map(
    async (address) => {
      const tx = await projectAdmin.addWhitelisted(address);
      console.log(tx.tx);
      return tx;
    })
  );
  console.log('Step 4 finished succesfully');
};
