/**
 * AGPL License
 * Circle of Angels aims to democratize social impact financing.
 * It facilitate the investment process by utilizing smart contracts to develop impact milestones agreed upon by funders and the social entrepenuers.
 *
 * Copyright (C) 2019 AtixLabs, S.R.L <https://www.atixlabs.com>
 */

// const config = require('./deploy_configs.js');

const whiteListedAddress = ['0x9e1ef1ec212f5dffb41d35d9e5c14054f26c6560'];

const COAProjectAdmin = artifacts.require('COAProjectAdmin');

module.exports = async (deployer) => {
  const projectAdmin = await COAProjectAdmin.deployed();
  console.log(`Whitelisting on project admin contract on address ${projectAdmin.address}`);
  const txAddress = await Promise.all(whiteListedAddress.map(
    async (address) => {
      const tx = await projectAdmin.addWhitelisted(address);
      console.log(tx.tx);
      return tx;
    })
  );
  console.log('Step 5 finished succesfully');
};
