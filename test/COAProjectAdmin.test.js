/**
 * AGPL License
 * Circle of Angels aims to democratize social impact financing.
 * It facilitate the investment process by utilizing smart contracts to develop impact milestones agreed upon by funders and the social entrepenuers.
 *
 * Copyright (C) 2019 AtixLabs, S.R.L <https://www.atixlabs.com>
 */

const COAProjectAdmin = artifacts.require('COAProjectAdmin');
const COAOracle = artifacts.require('COAOracle');
const Web3 = require('web3');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');

chai.use(chaiAsPromised);
chai.should();

// TODO: this could be put on a constants file
// Budget status
const CLAIMABLE = 1;
const CLAIMED = 2;
const FUNDED = 3;
const BLOCKED = 4;
// Validation status
const PENDING = 0;
const COMPLETED = 1;

const web3 = new Web3('http://localhost:8545');

const adminAddress = '0x20edcef7991c7a0640ce6b8938d41b3c7aa8cbee';
const seAddress = web3.utils.toChecksumAddress('0x20edcef7991c7a0640ce6b8938d41b3c7aa8cbee');
const oracleContractAddress = web3.utils.toChecksumAddress('0x20edcef7991c7a0640ce6b8938d41b3c7aa8cbee');
const notWhiteListedAddress = web3.utils.toChecksumAddress(
  '0x20edcef7991c7a0640ce6b8938d41b3c7ba8cbee');
contract('COAProjectAdmin Contract', function (accounts) {
  const owner = accounts[0];
  const oracleAccount = accounts[1];
  const seAccount = accounts[2];
  const oracleContractAddressMock = accounts[3];

  beforeEach(async function () {
    // Create Oracle contract
    this.oracleContract = await COAOracle.new({
      from: owner,
    });

    // Create admin Contract
    // const oracleContractDeployed = await COAOracle.deployed();
    this.oracleContractAddress = this.oracleContract.address;
    this.contract = await COAProjectAdmin.new(owner, this.oracleContractAddress, {
      from: owner,
    });

    await this.contract.addWhitelisted(owner);
    // await this.contract.addWhitelisted(seAccount);
  });

  describe('CAO Project Creation', function () {
    it('Create a project', async function () {
      await this.contract.createProject(1, seAddress, 'Test Project', {
        from: owner,
      });
      const id = await this.contract.getProject(1);
      const se = await this.contract.getSe(1);
      const name = await this.contract.getName(1);
      assert.equal(se, seAddress);
      assert.equal(id, 1);
      assert.equal(name, 'Test Project');
    });

    it('Error - Can\'t create a project from a not whitelisted address', async function () {
      await this.contract.createProject(1, seAddress, 'Test Project', {
        from: notWhiteListedAddress,
      }).should.be.rejectedWith('WhitelistedRole: caller does not have the Whitelisted role');
    });

    it('Error - Can\'t create an already existing project', async function () {
      await this.contract.createProject(1, seAddress, 'Test Project', {
        from: owner,
      });
      await this.contract.createProject(1, seAddress, 'Test Project', {
        from: owner,
      }).should.be.rejectedWith('Project already existed');
    });

    it('Create a milestone', async function () {
      await this.contract.createProject(1, seAddress, 'Test Project');
      await this.contract.createMilestone(1, 1, 1000, 'Make a better world');
      const milestoneDesc = await this.contract.getMilestone(1, 1);
      assert.equal(milestoneDesc, 'Make a better world');
    });

    it('Error - Can\'t create a milestone from a not whitelisted address', async function () {
      await this.contract.createProject(1, seAddress, 'Test Project');
      await this.contract.createMilestone(1, 1, 1000, 'Make a better world', {
        from: notWhiteListedAddress,
      }).should.be.rejectedWith('WhitelistedRole: caller does not have the Whitelisted role');
    });

    it('Error - Can\'t create an already existing milestone', async function () {
      await this.contract.createProject(1, seAddress, 'Test Project');
      await this.contract.createMilestone(1, 1, 1000, 'Make a better world');
      await this.contract.createMilestone(1, 1, 1000, 'Make a better world')
        .should.be.rejectedWith('Milestone already existed');
    });

    it('Error - Can\'t create a milestone when project started', async function () {
      await this.contract.createProject(1, seAddress, 'Test Project');
      await this.contract.createMilestone(1, 1, 1000, 'Make a better world');
      await this.contract.createActivity(1, 1, 1, oracleAccount, 'Build a school');
      await this.contract.startProject(1);
      await this.contract.createMilestone(2, 1, 1000, 'Make a better world 2')
        .should.be.rejectedWith('Project already started');
    });

    it('Create a activity', async function () {
      await this.contract.createProject(1, seAddress, 'Test Project');
      await this.contract.createMilestone(1, 1, 1000, 'Make a better world');
      await this.contract.createActivity(1, 1, 1, oracleAccount, 'Build a school');

      const activityState = await this.contract.getActivityState(1, 1, 1);
      assert.equal(activityState, 0);
    });

    it('Error - Can\'t create a activity from a not whitelisted address', async function () {
      await this.contract.createProject(1, seAddress, 'Test Project');
      await this.contract.createMilestone(1, 1, 1000, 'Make a better world');
      await this.contract.createActivity(1, 1, 1, oracleAccount, 'Build a school', {
        from: notWhiteListedAddress,
      }).should.be.rejectedWith('WhitelistedRole: caller does not have the Whitelisted role');
    });

    it('Error - Can\'t create a activity when project started', async function () {
      await this.contract.createProject(1, seAddress, 'Test Project');
      await this.contract.createMilestone(1, 1, 1000, 'Make a better world');
      await this.contract.createActivity(1, 1, 1, oracleAccount, 'Build a school');
      await this.contract.startProject(1);
      await this.contract.createActivity(2, 1, 1, oracleAccount, 'Build a school')
        .should.be.rejectedWith('Project already started');
    });

    it('Error - Can\'t create an already existing activity', async function () {
      await this.contract.createProject(1, seAddress, 'Test Project');
      await this.contract.createMilestone(1, 1, 1000, 'Make a better world');
      await this.contract.createActivity(1, 1, 1, oracleAccount, 'Build a school');
      await this.contract.createActivity(1, 1, 1, oracleAccount, 'Build a school')
        .should.be.rejectedWith('Activity already existed');
    });
  });

  describe('CAO Activity Validation', function () {
    it('Validate an activity', async function () {
      await this.contract.createProject(1, seAddress, 'Test Project');
      await this.contract.createMilestone(1, 1, 1000, 'Make a better world');
      await this.contract.createActivity(1, 1, 1, oracleAccount, 'Build a school');

      const activityState = await this.contract.getActivityState(1, 1, 1);
      assert.equal(activityState, 0);

      // setting white listing to a mock address because the oracle contract account is not
      // created in ganache and fails when you sent a transaction from that address
      await this.contract.setAdminSCAddress(oracleContractAddressMock, { from: owner });

      await this.contract.validateActivity(1, 1, 1, {
        from: oracleContractAddressMock,
      });
      const newActivityState = await this.contract.getActivityState(1, 1, 1);
      assert.equal(newActivityState, 1);
    });
  });

  describe('CAO Milestone completed', function () {
    it('Mark milestone as completed when all activities are validated', async function () {
      await this.contract.createProject(1, seAddress, 'Test Project');
      await this.contract.createMilestone(1, 1, 1000, 'Make a better world');
      await this.contract.createActivity(1, 1, 1, oracleAccount, 'Build a school');
      await this.contract.createActivity(2, 1, 1, oracleAccount, 'Make a change');
      await this.contract.createActivity(3, 1, 1, oracleAccount, 'Help others');

      const activity1State = await this.contract.getActivityState(1, 1, 1);
      assert.equal(activity1State, 0);
      const activity2State = await this.contract.getActivityState(1, 1, 2);
      assert.equal(activity2State, 0);
      const activity3State = await this.contract.getActivityState(1, 1, 3);
      assert.equal(activity3State, 0);

      const milestoneState = await this.contract.getMilestoneState(1, 1);
      assert.equal(milestoneState, 0);

      // setting white listing to a mock address because the oracle contract account is not
      // created in ganache and fails when you sent a transaction from that address
      await this.contract.setAdminSCAddress(oracleContractAddressMock);

      // validate activities
      await this.contract.validateActivity(1, 1, 1, {
        from: oracleContractAddressMock,
      });
      await this.contract.validateActivity(1, 1, 2, {
        from: oracleContractAddressMock,
      });
      await this.contract.validateActivity(1, 1, 3, {
        from: oracleContractAddressMock,
      });
      const newActivity1State = await this.contract.getActivityState(1, 1, 1);
      assert.equal(newActivity1State, 1);
      const newActivity2State = await this.contract.getActivityState(1, 1, 2);
      assert.equal(newActivity2State, 1);
      const newActivity3State = await this.contract.getActivityState(1, 1, 3);
      assert.equal(newActivity3State, 1);

      const newMilestoneState = await this.contract.getMilestoneState(1, 1);
      assert.equal(newMilestoneState, 1);
    });

    it('Don\'t mark milestone as completed with 1 activity is not validated', async function () {
      await this.contract.createProject(1, seAddress, 'Test Project');
      await this.contract.createMilestone(1, 1, 1000, 'Make a better world');
      await this.contract.createActivity(1, 1, 1, oracleAccount, 'Build a school');
      await this.contract.createActivity(2, 1, 1, oracleAccount, 'Make a change');
      await this.contract.createActivity(3, 1, 1, oracleAccount, 'Help others');

      const activity1State = await this.contract.getActivityState(1, 1, 1);
      assert.equal(activity1State, 0);
      const activity2State = await this.contract.getActivityState(1, 1, 2);
      assert.equal(activity2State, 0);
      const activity3State = await this.contract.getActivityState(1, 1, 3);
      assert.equal(activity3State, 0);

      const milestoneState = await this.contract.getMilestoneState(1, 1);
      assert.equal(milestoneState, 0);

      // setting white listing to a mock address because the oracle contract account is not
      // created in ganache and fails when you sent a transaction from that address
      await this.contract.setAdminSCAddress(oracleContractAddressMock);

      // validate activities
      await this.contract.validateActivity(1, 1, 1, {
        from: oracleContractAddressMock,
      });
      await this.contract.validateActivity(1, 1, 2, {
        from: oracleContractAddressMock,
      });

      const newActivity1State = await this.contract.getActivityState(1, 1, 1);
      assert.equal(newActivity1State, 1);
      const newActivity2State = await this.contract.getActivityState(1, 1, 2);
      assert.equal(newActivity2State, 1);
      const newActivity3State = await this.contract.getActivityState(1, 1, 3);
      assert.equal(newActivity3State, 0);

      const newMilestoneState = await this.contract.getMilestoneState(1, 1);
      assert.equal(newMilestoneState, 0);
    });
  });

  describe('COA Project Start', function () {
    it('Start a project', async function () {
      await this.contract.createProject(1, seAddress, 'Test Project');
      await this.contract.createMilestone(1, 1, 1000, 'Make a better world');
      await this.contract.createActivity(1, 1, 1, oracleAccount, 'Build a school');
      await this.contract.createMilestone(2, 1, 1000, 'Make a better world');
      await this.contract.createActivity(2, 2, 1, oracleAccount, 'Build a school');
      await this.contract.startProject(1);
    });

    it('Don\'t allow start a started project', async function () {
      await this.contract.createProject(1, seAddress, 'Test Project');
      await this.contract.createMilestone(1, 1, 1000, 'Make a better world');
      await this.contract.createActivity(1, 1, 1, oracleAccount, 'Build a school');
      await this.contract.createMilestone(2, 1, 1000, 'Make a better world');
      await this.contract.createActivity(2, 2, 1, oracleAccount, 'Build a school');
      await this.contract.startProject(1);
      await this.contract.startProject(1).should.be.rejectedWith('Project already started');
    });

    it('Error - Can\'t start a project without milestones', async function () {
      await this.contract.createProject(1, seAddress, 'Test Project');
      await this.contract.startProject(1).should.be.rejectedWith('Project doesn\'t have milestones');
    });

    it('Error - Can\'t start a project with milestone without activities', async function () {
      await this.contract.createProject(1, seAddress, 'Test Project');
      await this.contract.createMilestone(1, 1, 1000, 'Make a better world');
      await this.contract.startProject(1).should.be.rejectedWith('Milestone doesn\'t have activities');
    });
  });

  describe('COA Project completed', function () {
    it('Mark project as completed when all milestones are validated and funded', async function () {
      await this.contract.createProject(1, seAccount, 'Test Project');
      await this.contract.createMilestone(1, 1, 1000, 'Make a better world');
      await this.contract.createActivity(1, 1, 1, oracleAccount, 'Build a school');
      await this.contract.createActivity(2, 1, 1, oracleAccount, 'Make a change');
      await this.contract.createActivity(3, 1, 1, oracleAccount, 'Help others');
      await this.contract.createMilestone(2, 1, 1000, 'Make a better world');
      await this.contract.createActivity(4, 2, 1, oracleAccount, 'Build a school');
      await this.contract.createActivity(5, 2, 1, oracleAccount, 'Make a change');
      await this.contract.createActivity(6, 2, 1, oracleAccount, 'Help others');
      await this.contract.startProject(1);

      const activity1State = await this.contract.getActivityState(1, 1, 1);
      assert.equal(activity1State, 0);
      const activity2State = await this.contract.getActivityState(1, 1, 2);
      assert.equal(activity2State, 0);
      const activity3State = await this.contract.getActivityState(1, 1, 3);
      assert.equal(activity3State, 0);

      const milestone1State = await this.contract.getMilestoneState(1, 1);
      assert.equal(milestone1State, 0);

      const activity4State = await this.contract.getActivityState(1, 1, 4);
      assert.equal(activity4State, 0);
      const activity5State = await this.contract.getActivityState(1, 1, 5);
      assert.equal(activity5State, 0);
      const activity6State = await this.contract.getActivityState(1, 1, 6);
      assert.equal(activity6State, 0);

      const milestone2State = await this.contract.getMilestoneState(1, 2);
      assert.equal(milestone2State, 0);

      const projectState = await this.contract.getProjectState(1);
      assert.equal(projectState, 0);

      // setting white listing to a mock address because the oracle contract account is not
      // created in ganache and fails when you sent a transaction from that address
      await this.contract.setAdminSCAddress(oracleContractAddressMock);

      // validate activities
      await this.contract.validateActivity(1, 1, 1, {
        from: oracleContractAddressMock,
      });
      await this.contract.validateActivity(1, 1, 2, {
        from: oracleContractAddressMock,
      });
      await this.contract.validateActivity(1, 1, 3, {
        from: oracleContractAddressMock,
      });

      await this.contract.validateActivity(1, 2, 4, {
        from: oracleContractAddressMock,
      });
      await this.contract.validateActivity(1, 2, 5, {
        from: oracleContractAddressMock,
      });
      await this.contract.validateActivity(1, 2, 6, {
        from: oracleContractAddressMock,
      });

      const newActivity1State = await this.contract.getActivityState(1, 1, 1);
      assert.equal(newActivity1State, 1);
      const newActivity2State = await this.contract.getActivityState(1, 1, 2);
      assert.equal(newActivity2State, 1);
      const newActivity3State = await this.contract.getActivityState(1, 1, 3);
      assert.equal(newActivity3State, 1);

      const newMilestoneState = await this.contract.getMilestoneState(1, 1);
      assert.equal(newMilestoneState, 1);

      const newActivity4State = await this.contract.getActivityState(1, 2, 4);
      assert.equal(newActivity4State, 1);
      const newActivity5State = await this.contract.getActivityState(1, 2, 5);
      assert.equal(newActivity5State, 1);
      const newActivity6State = await this.contract.getActivityState(1, 2, 6);
      assert.equal(newActivity6State, 1);

      const newMilestone2State = await this.contract.getMilestoneState(1, 2);
      assert.equal(newMilestone2State, 1);

      const projectStateWithMilestonesNotClaimed = await this.contract.isProjectCompleted(1);
      assert.equal(projectStateWithMilestonesNotClaimed, false);

      await this.contract.claimMilestone(1, 1, {
        from: seAccount,
      });

      await this.contract.setMilestoneFunded(1, 1, {
        from: oracleContractAddressMock,
      });

      await this.contract.claimMilestone(2, 1, {
        from: seAccount,
      });

      await this.contract.setMilestoneFunded(2, 1, {
        from: oracleContractAddressMock,
      });

      const newProjectState = await this.contract.isProjectCompleted(1);
      assert.equal(newProjectState, true);
    });

    it('Don\'t mark project as completed when all milestones are validated but not funded', async function () {
      await this.contract.createProject(1, seAddress, 'Test Project');
      await this.contract.createMilestone(1, 1, 1000, 'Make a better world');
      await this.contract.createActivity(1, 1, 1, oracleAccount, 'Build a school');
      await this.contract.createActivity(2, 1, 1, oracleAccount, 'Make a change');
      await this.contract.createActivity(3, 1, 1, oracleAccount, 'Help others');
      await this.contract.createMilestone(2, 1, 1000, 'Make a better world');
      await this.contract.createActivity(4, 2, 1, oracleAccount, 'Build a school');
      await this.contract.createActivity(5, 2, 1, oracleAccount, 'Make a change');
      await this.contract.createActivity(6, 2, 1, oracleAccount, 'Help others');

      const activity1State = await this.contract.getActivityState(1, 1, 1);
      assert.equal(activity1State, 0);
      const activity2State = await this.contract.getActivityState(1, 1, 2);
      assert.equal(activity2State, 0);
      const activity3State = await this.contract.getActivityState(1, 1, 3);
      assert.equal(activity3State, 0);

      const milestone1State = await this.contract.getMilestoneState(1, 1);
      assert.equal(milestone1State, 0);

      const activity4State = await this.contract.getActivityState(1, 1, 4);
      assert.equal(activity4State, 0);
      const activity5State = await this.contract.getActivityState(1, 1, 5);
      assert.equal(activity5State, 0);
      const activity6State = await this.contract.getActivityState(1, 1, 6);
      assert.equal(activity6State, 0);

      const milestone2State = await this.contract.getMilestoneState(1, 2);
      assert.equal(milestone2State, 0);

      const projectState = await this.contract.getProjectState(1);
      assert.equal(projectState, 0);

      // setting white listing to a mock address because the oracle contract account is not
      // created in ganache and fails when you sent a transaction from that address
      await this.contract.setAdminSCAddress(oracleContractAddressMock);

      // validate activities
      await this.contract.validateActivity(1, 1, 1, {
        from: oracleContractAddressMock,
      });
      await this.contract.validateActivity(1, 1, 2, {
        from: oracleContractAddressMock,
      });
      await this.contract.validateActivity(1, 1, 3, {
        from: oracleContractAddressMock,
      });

      await this.contract.validateActivity(1, 2, 4, {
        from: oracleContractAddressMock,
      });
      await this.contract.validateActivity(1, 2, 5, {
        from: oracleContractAddressMock,
      });
      await this.contract.validateActivity(1, 2, 6, {
        from: oracleContractAddressMock,
      });

      const newActivity1State = await this.contract.getActivityState(1, 1, 1);
      assert.equal(newActivity1State, 1);
      const newActivity2State = await this.contract.getActivityState(1, 1, 2);
      assert.equal(newActivity2State, 1);
      const newActivity3State = await this.contract.getActivityState(1, 1, 3);
      assert.equal(newActivity3State, 1);

      const newMilestoneState = await this.contract.getMilestoneState(1, 1);
      assert.equal(newMilestoneState, 1);

      const newActivity4State = await this.contract.getActivityState(1, 2, 4);
      assert.equal(newActivity4State, 1);
      const newActivity5State = await this.contract.getActivityState(1, 2, 5);
      assert.equal(newActivity5State, 1);
      const newActivity6State = await this.contract.getActivityState(1, 2, 6);
      assert.equal(newActivity6State, 1);

      const newMilestone2State = await this.contract.getMilestoneState(1, 2);
      assert.equal(newMilestone2State, 1);

      // project is still not marked as completed because the budget status of the milestones
      const newProjectState = await this.contract.getProjectState(1);
      assert.equal(newProjectState, 0);
    });

    it('Don\'t mark project as completed when all milestones are validated but not funded', async function () {
      await this.contract.createProject(1, seAddress, 'Test Project');
      await this.contract.createMilestone(1, 1, 1000, 'Make a better world');
      await this.contract.createActivity(1, 1, 1, oracleAccount, 'Build a school');
      await this.contract.createActivity(2, 1, 1, oracleAccount, 'Make a change');
      await this.contract.createActivity(3, 1, 1, oracleAccount, 'Help others');
      await this.contract.createMilestone(2, 1, 1000, 'Make a better world');
      await this.contract.createActivity(4, 2, 1, oracleAccount, 'Build a school');
      await this.contract.createActivity(5, 2, 1, oracleAccount, 'Make a change');
      await this.contract.createActivity(6, 2, 1, oracleAccount, 'Help others');

      const activity1State = await this.contract.getActivityState(1, 1, 1);
      assert.equal(activity1State, 0);
      const activity2State = await this.contract.getActivityState(1, 1, 2);
      assert.equal(activity2State, 0);
      const activity3State = await this.contract.getActivityState(1, 1, 3);
      assert.equal(activity3State, 0);

      const milestone1State = await this.contract.getMilestoneState(1, 1);
      assert.equal(milestone1State, 0);

      const activity4State = await this.contract.getActivityState(1, 1, 4);
      assert.equal(activity4State, 0);
      const activity5State = await this.contract.getActivityState(1, 1, 5);
      assert.equal(activity5State, 0);
      const activity6State = await this.contract.getActivityState(1, 1, 6);
      assert.equal(activity6State, 0);

      const milestone2State = await this.contract.getMilestoneState(1, 2);
      assert.equal(milestone2State, 0);

      const projectState = await this.contract.getProjectState(1);
      assert.equal(projectState, 0);

      // setting white listing to a mock address because the oracle contract account is not
      // created in ganache and fails when you sent a transaction from that address
      await this.contract.setAdminSCAddress(oracleContractAddressMock);

      // validate activities
      await this.contract.validateActivity(1, 1, 1, {
        from: oracleContractAddressMock,
      });
      await this.contract.validateActivity(1, 1, 2, {
        from: oracleContractAddressMock,
      });
      await this.contract.validateActivity(1, 1, 3, {
        from: oracleContractAddressMock,
      });

      await this.contract.validateActivity(1, 2, 4, {
        from: oracleContractAddressMock,
      });
      await this.contract.validateActivity(1, 2, 5, {
        from: oracleContractAddressMock,
      });
      await this.contract.validateActivity(1, 2, 6, {
        from: oracleContractAddressMock,
      });

      const newActivity1State = await this.contract.getActivityState(1, 1, 1);
      assert.equal(newActivity1State, 1);
      const newActivity2State = await this.contract.getActivityState(1, 1, 2);
      assert.equal(newActivity2State, 1);
      const newActivity3State = await this.contract.getActivityState(1, 1, 3);
      assert.equal(newActivity3State, 1);

      const newMilestoneState = await this.contract.getMilestoneState(1, 1);
      assert.equal(newMilestoneState, 1);

      const newActivity4State = await this.contract.getActivityState(1, 2, 4);
      assert.equal(newActivity4State, 1);
      const newActivity5State = await this.contract.getActivityState(1, 2, 5);
      assert.equal(newActivity5State, 1);
      const newActivity6State = await this.contract.getActivityState(1, 2, 6);
      assert.equal(newActivity6State, 1);

      const newMilestone2State = await this.contract.getMilestoneState(1, 2);
      assert.equal(newMilestone2State, 1);

      // project is still not marked as completed because the budget status of the milestones
      const newProjectState = await this.contract.getProjectState(1);
      assert.equal(newProjectState, 0);
    });

    it('Don\'t mark project as completed with 1 activity is not validated', async function () {
      await this.contract.createProject(1, seAddress, 'Test Project');
      await this.contract.createMilestone(1, 1, 1000, 'Make a better world');
      await this.contract.createActivity(1, 1, 1, oracleAccount, 'Build a school');
      await this.contract.createActivity(2, 1, 1, oracleAccount, 'Make a change');
      await this.contract.createActivity(3, 1, 1, oracleAccount, 'Help others');
      await this.contract.createMilestone(2, 1, 1000, 'Make a better world');
      await this.contract.createActivity(4, 2, 1, oracleAccount, 'Build a school');
      await this.contract.createActivity(5, 2, 1, oracleAccount, 'Make a change');
      await this.contract.createActivity(6, 2, 1, oracleAccount, 'Help others');

      const activity1State = await this.contract.getActivityState(1, 1, 1);
      assert.equal(activity1State, 0);
      const activity2State = await this.contract.getActivityState(1, 1, 2);
      assert.equal(activity2State, 0);
      const activity3State = await this.contract.getActivityState(1, 1, 3);
      assert.equal(activity3State, 0);

      const milestone1State = await this.contract.getMilestoneState(1, 1);
      assert.equal(milestone1State, 0);

      const activity4State = await this.contract.getActivityState(1, 1, 4);
      assert.equal(activity4State, 0);
      const activity5State = await this.contract.getActivityState(1, 1, 5);
      assert.equal(activity5State, 0);
      const activity6State = await this.contract.getActivityState(1, 1, 6);
      assert.equal(activity6State, 0);

      const milestone2State = await this.contract.getMilestoneState(1, 2);
      assert.equal(milestone2State, 0);

      const projectState = await this.contract.getProjectState(1);
      assert.equal(projectState, 0);

      // setting white listing to a mock address because the oracle contract account is not
      // created in ganache and fails when you sent a transaction from that address
      await this.contract.setAdminSCAddress(oracleContractAddressMock);

      // validate activities
      await this.contract.validateActivity(1, 1, 1, {
        from: oracleContractAddressMock,
      });
      await this.contract.validateActivity(1, 1, 2, {
        from: oracleContractAddressMock,
      });
      await this.contract.validateActivity(1, 1, 3, {
        from: oracleContractAddressMock,
      });

      await this.contract.validateActivity(1, 2, 4, {
        from: oracleContractAddressMock,
      });
      await this.contract.validateActivity(1, 2, 5, {
        from: oracleContractAddressMock,
      });

      // activity 6 is not validated

      const newActivity1State = await this.contract.getActivityState(1, 1, 1);
      assert.equal(newActivity1State, 1);
      const newActivity2State = await this.contract.getActivityState(1, 1, 2);
      assert.equal(newActivity2State, 1);
      const newActivity3State = await this.contract.getActivityState(1, 1, 3);
      assert.equal(newActivity3State, 1);

      const newMilestoneState = await this.contract.getMilestoneState(1, 1);
      assert.equal(newMilestoneState, 1);

      const newActivity4State = await this.contract.getActivityState(1, 2, 4);
      assert.equal(newActivity4State, 1);
      const newActivity5State = await this.contract.getActivityState(1, 2, 5);
      assert.equal(newActivity5State, 1);
      const newActivity6State = await this.contract.getActivityState(1, 2, 6);
      assert.equal(newActivity6State, 0);

      // milestone 2 is not completed because of activity 6

      const newMilestone2State = await this.contract.getMilestoneState(1, 2);
      assert.equal(newMilestone2State, 0);

      const newProjectState = await this.contract.getProjectState(1);
      assert.equal(newProjectState, 0);
    });
  });

  describe('Milestone budget status', function () {
    it('Mark first milestone as claimable when project starts', async function () {
      await this.contract.createProject(1, seAddress, 'Test Project');
      await this.contract.createMilestone(1, 1, 1000, 'Make a better world');
      await this.contract.createActivity(1, 1, 1, oracleAccount, 'Build a school');
      await this.contract.createActivity(2, 1, 1, oracleAccount, 'Make a change');
      await this.contract.createActivity(3, 1, 1, oracleAccount, 'Help others');
      await this.contract.createMilestone(2, 1, 1000, 'Make a better world');
      await this.contract.createActivity(4, 2, 1, oracleAccount, 'Build a school');
      await this.contract.createActivity(5, 2, 1, oracleAccount, 'Make a change');
      await this.contract.createActivity(6, 2, 1, oracleAccount, 'Help others');

      const tx = await this.contract.startProject(1);

      // check events
      const milestoneEvents = tx.logs.filter(it => it.event === 'MilestoneClaimable' || it.event === 'ProjectStarted');
      // 1 MilestoneClaimable event detected
      assert.equal(milestoneEvents.length, 2);

      const milestoneEventsArgs = milestoneEvents.map(it => it.args);
      assert.equal(milestoneEventsArgs[0].id, 1);
      assert.equal(milestoneEventsArgs[1].id, 1);
    });

    it('All milestone are blocked if project doesn\'t start', async function () {
      await this.contract.createProject(1, seAddress, 'Test Project');
      await this.contract.createMilestone(1, 1, 1000, 'Make a better world');
      await this.contract.createActivity(1, 1, 1, oracleAccount, 'Build a school');
      await this.contract.createActivity(2, 1, 1, oracleAccount, 'Make a change');
      await this.contract.createActivity(3, 1, 1, oracleAccount, 'Help others');
      await this.contract.createMilestone(2, 1, 1000, 'Make a better world');
      await this.contract.createActivity(4, 2, 1, oracleAccount, 'Build a school');
      await this.contract.createActivity(5, 2, 1, oracleAccount, 'Make a change');
      await this.contract.createActivity(6, 2, 1, oracleAccount, 'Help others');
      const budgetStatus1 = await this.contract.getMilestoneBudgetStatus(1, 1);
      const budgetStatus2 = await this.contract.getMilestoneBudgetStatus(1, 2);

      // milestones are blocked
      assert.equal(budgetStatus1, 4);
      assert.equal(budgetStatus2, 4);
    });

    it('Second milestone get mark as claimable once the first milestone is funded and completed', async function () {
      await this.contract.createProject(1, seAccount, 'Test Project');
      await this.contract.createMilestone(1, 1, 1000, 'Make a better world');
      await this.contract.createActivity(1, 1, 1, oracleAccount, 'Build a school');
      await this.contract.createMilestone(2, 1, 1000, 'Make a better world');
      await this.contract.createActivity(2, 2, 1, oracleAccount, 'Build a school');

      await this.contract.startProject(1);

      const budgetStatus2 = await this.contract.getMilestoneBudgetStatus(1, 2);

      // second milestone is blocked
      assert.equal(budgetStatus2, 4);

      // setting white listing to a mock address because the oracle contract account is not
      // created in ganache and fails when you sent a transaction from that address
      await this.contract.setAdminSCAddress(oracleContractAddressMock);

      // validate activity of milestone 1, milestone is completed
      await this.contract.validateActivity(1, 1, 1, {
        from: oracleContractAddressMock,
      });

      await this.contract.claimMilestone(1, 1, {
        from: seAccount,
      });

      await this.contract.setMilestoneFunded(1, 1, {
        from: oracleContractAddressMock,
      });

      const newBudgetStatus2 = await this.contract.getMilestoneBudgetStatus(1, 2);
      // second milestone is claimable
      assert.equal(newBudgetStatus2, 1);
    });
    it('Mark a completed milestone as claimable just when the ' +
        'previous milestones were claimed and funded', async function () {
      await this.contract.createProject(1, seAccount, 'Test Project');
      await this.contract.createMilestone(1, 1, 1000, 'Make a better world 1');
      await this.contract.createActivity(1, 1, 1, oracleAccount, 'Build a school 1');
      await this.contract.createMilestone(2, 1, 1000, 'Make a better world 2');
      await this.contract.createActivity(2, 2, 1, oracleAccount, 'Build a school 2');
      await this.contract.createMilestone(3, 1, 1000, 'Make a better world 3');
      await this.contract.createActivity(3, 3, 1, oracleAccount, 'Build a school 3');

      // all milestones and activities are pending
      const activity1State = await this.contract.getActivityState(1, 1, 1);
      assert.equal(activity1State, PENDING);

      const milestone1State = await this.contract.getMilestoneState(1, 1);
      assert.equal(milestone1State, PENDING);

      const activity2State = await this.contract.getActivityState(1, 2, 2);
      assert.equal(activity2State, PENDING);

      const milestone2State = await this.contract.getMilestoneState(1, 2);
      assert.equal(milestone2State, PENDING);

      const activity3State = await this.contract.getActivityState(1, 3, 3);
      assert.equal(activity3State, PENDING);

      const milestone3State = await this.contract.getMilestoneState(1, 3);
      assert.equal(milestone3State, PENDING);

      const projectState = await this.contract.getProjectState(1);
      assert.equal(projectState, PENDING);

      // setting white listing to a mock address because the oracle contract account is not
      // created in ganache and fails when you sent a transaction from that address
      await this.contract.setAdminSCAddress(oracleContractAddressMock);

      await this.contract.startProject(1);
      // first milestone is claimable
      const budgetStatus1 = await this.contract.getMilestoneBudgetStatus(1, 1);
      assert.equal(budgetStatus1, CLAIMABLE);
      // second milestone is blocked
      const budgetStatus2 = await this.contract.getMilestoneBudgetStatus(1, 2);
      assert.equal(budgetStatus2, BLOCKED);
      // third milestone is blocked
      const budgetStatus3 = await this.contract.getMilestoneBudgetStatus(1, 3);
      assert.equal(budgetStatus3, BLOCKED);

      // milestone 1 is claimed
      await this.contract.claimMilestone(1, 1, {
        from: seAccount,
      });
      const newBudgetStatus1Claimed = await this.contract.getMilestoneBudgetStatus(1, 1);
      assert.equal(newBudgetStatus1Claimed, CLAIMED);

      // milestone 1 is funded, milestone 2 and 3 remains blocked
      await this.contract.setMilestoneFunded(1, 1, {
        from: oracleContractAddressMock,
      });
      const newBudgetStatus1Funded = await this.contract.getMilestoneBudgetStatus(1, 1);
      assert.equal(newBudgetStatus1Funded, FUNDED);
      const newBudgetStatus2Blocked = await this.contract.getMilestoneBudgetStatus(1, 2);
      assert.equal(newBudgetStatus2Blocked, BLOCKED);
      const newBudgetStatus3Blocked = await this.contract.getMilestoneBudgetStatus(1, 3);
      assert.equal(newBudgetStatus3Blocked, BLOCKED);

      // activity 1 is validated
      await this.contract.validateActivity(1, 1, 1, {
        from: oracleContractAddressMock,
      });

      // milestone 1 is completed
      const milestone1StateCompleted = await this.contract.getMilestoneState(1, 1);
      assert.equal(milestone1StateCompleted, COMPLETED);
      // milestone 2 is claimable
      const budgetStatus2Claimable = await this.contract.getMilestoneBudgetStatus(1, 2);
      assert.equal(budgetStatus2Claimable, CLAIMABLE);
      // milestone 3 remains blocked
      const budgetStatus3Blocked = await this.contract.getMilestoneBudgetStatus(1, 3);
      assert.equal(budgetStatus3Blocked, BLOCKED);

      // activity 3 is validated
      await this.contract.validateActivity(1, 3, 3, {
        from: oracleContractAddressMock,
      });
      // milestone 3 is completed
      const milestone3StateCompleted = await this.contract.getMilestoneState(1, 3);
      assert.equal(milestone3StateCompleted, COMPLETED);
      // milestone 3 remains blocked
      const newBudgetStatus3CompletedButBlocked = await this.contract.getMilestoneBudgetStatus(1, 3);
      assert.equal(newBudgetStatus3CompletedButBlocked, BLOCKED);
      // milestone 2 is pending
      const milestone2StatePending = await this.contract.getMilestoneState(1, 2);
      assert.equal(milestone2StatePending, PENDING);
      // milestone 2 remains claimable
      const newBudgetStatus2Claimable = await this.contract.getMilestoneBudgetStatus(1, 2);
      assert.equal(newBudgetStatus2Claimable, CLAIMABLE);

      // milestone 2 is claimed
      await this.contract.claimMilestone(2, 1, {
        from: seAccount,
      });
      const newBudgetStatus2Claimed = await this.contract.getMilestoneBudgetStatus(1, 2);
      assert.equal(newBudgetStatus2Claimed, CLAIMED);
      // milestone 3 remains blocked
      const newBudgetStatus3BlockedM2Claimed = await this.contract.getMilestoneBudgetStatus(1, 3);
      assert.equal(newBudgetStatus3BlockedM2Claimed, BLOCKED);

      // milestone 2 is funded and pending
      await this.contract.setMilestoneFunded(2, 1, {
        from: oracleContractAddressMock,
      });
      const newBudgetStatus2Funded = await this.contract.getMilestoneBudgetStatus(1, 2);
      assert.equal(newBudgetStatus2Funded, FUNDED);
      const milestone2StatePendingAndFunded = await this.contract.getMilestoneState(1, 2);
      assert.equal(milestone2StatePendingAndFunded, PENDING);

      // activity 2 is validated
      await this.contract.validateActivity(1, 2, 2, {
        from: oracleContractAddressMock,
      });
      // milestone 2 is completed
      const milestone2StateCompleted = await this.contract.getMilestoneState(1, 2);
      assert.equal(milestone2StateCompleted, COMPLETED);
      // milestone 3 is claimable
      const budgetStatus3Claimable = await this.contract.getMilestoneBudgetStatus(1, 3);
      assert.equal(budgetStatus3Claimable, CLAIMABLE);

      // milestone 3 is claimed
      await this.contract.claimMilestone(3, 1, {
        from: seAccount,
      });
      const budgetStatus3Claimed = await this.contract.getMilestoneBudgetStatus(1, 3);
      assert.equal(budgetStatus3Claimed, CLAIMED);

      // milestone 3 is funded
      const txProjectCompleted = await this.contract.setMilestoneFunded(3, 1, {
        from: oracleContractAddressMock,
      });

      // check events
      const projectCompleted = txProjectCompleted.logs.filter(it => it.event === 'ProjectCompleted');
      // 1 ProjectCompleted event detected
      assert.equal(projectCompleted.length, 1);

      const projectArgs = projectCompleted.map(it => it.args);
      assert.equal(projectArgs[0].id, 1);
    });
  });
});
