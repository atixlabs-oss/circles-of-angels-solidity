/**
 * AGPL License
 * Circle of Angels aims to democratize social impact financing.
 * It facilitate the investment process by utilizing smart contracts to develop impact milestones agreed upon by funders and the social entrepenuers.
 *
 * Copyright (C) 2019 AtixLabs, S.R.L <https://www.atixlabs.com>
 */

const COAOracle = artifacts.require("COAOracle");
const COAProjectAdmin = artifacts.require("COAProjectAdmin");

const Web3 = require("web3");
const chai = require("chai");
const chaiAsPromised = require("chai-as-promised");

chai.use(chaiAsPromised);
chai.should();

const web3 = new Web3("http://localhost:8545");

const adminAddress = '0x20edcef7991c7a0640ce6b8938d41b3c7aa8cbee';

const seAddress = web3.utils.toChecksumAddress(
  "0x20edcef7991c7a0640ce6b8938d41b3c7aa8cbee"
);
const adminContractAddress = web3.utils.toChecksumAddress(
  "0x20edcef7991c7a0640ce6b8938d41b3c7aa8cbee"
);

const toBytes64Array = array => {
  array = array.map(row => row.split("").map(c => web3.utils.fromAscii(c)));
  return array;
};

const toStringArray = array => {
  array = array.map(row => row.map(c => web3.utils.toAscii(c)).join(""));
  return array;
};

// const oracleContractAddress = web3.utils.toChecksumAddress('0x20edcef7991c7a0640ce6b8938d41b3c7aa8cbee');
// const oracleAddress = web3.utils.toChecksumAddress('0x322BF4182375dc1bB4fbb1F79c81925A04B336dE');

// const oracleAccount = web3.eth.accounts.create(entropy);

contract('COAOracle Contract', function (accounts) {
  const owner = accounts[0];
  const oracleAccount = accounts[1];
  const seAccount = accounts[2];

  beforeEach(async function () {
    // Create oracle contract
    this.contract = await COAOracle.new();
    // Create admin contract with oracle contract addres
    this.contractAdmin = await COAProjectAdmin.new(
      adminAddress,
      this.contract.address
    );

    // Set admin contrac address on oracle contract
    await this.contract.setAdminSCAddress(this.contractAdmin.address);
    // add admin to the white list
    await this.contractAdmin.addWhitelisted(owner);
  });

  describe('CAO Activity Validation', function () {
    it.only('Validate an activity', async function () {
      await this.contract.createActivity(
        1,
        1,
        1,
        oracleAccount,
        'Make a better world', {
          from: this.contractAdmin.address,
        }
      );

      const activityState = await this.contract.getActivityState(1);
      assert.equal(activityState, 0);
      await this.contract.validateActivity(1, {
        from: oracleAccount,
      });
      const newActivityState = await this.contract.getActivityState(1);
      assert.equal(newActivityState, 1);
    });

    it('Error - Can\'t validate an activity that doesn\'t exist', async function () {
      await this.contract.validateActivity(1, {
        from: oracleAccount,
      }).should.be.rejectedWith('Activity doesn\'t exist');
    });

    it('Error - Can\'t validate an activity twice', async function () {
      await this.contract.createActivity(
        1,
        1,
        1,
        oracleAccount,
        'Make a better world'
      );

      const activityState = await this.contract.getActivityState(1);
      assert.equal(activityState, 0);
      await this.contract.validateActivity(1, {
        from: oracleAccount,
      });
      const newActivityState = await this.contract.getActivityState(1);
      assert.equal(newActivityState, 1);

      await this.contract.validateActivity(1, {
        from: oracleAccount,
      }).should.be.rejectedWith('Activity already validated');
    });

    it('Error - Validate an activity with a wrong oracle', async function () {
      await this.contract.createActivity(
        1,
        1,
        1,
        oracleAccount,
        'Build a school'
      );

      const activityState = await this.contract.getActivityState(1);
      assert.equal(activityState, 0);
      await this.contract
        .validateActivity(1, {
          from: seAccount,
        })
        .should.be.rejectedWith('Invalid oracle');
    });

    it('Get an activity oracle', async function () {
      await this.contract.createActivity(
        1,
        1,
        1,
        oracleAccount,
        'Build a school'
      );

      const activityState = await this.contract.getActivityState(1);
      assert.equal(activityState, 0);

      const oracle = await this.contract.getActivityOracle(1);

      assert.equal(oracle, oracleAccount);
    });

    it('Upload evidences to existent activity being oracle', async function () {
      await this.contract.createActivity(
        1,
        1,
        1,
        oracleAccount,
        'Walk on moon'
      );
      const actualHashes = await this.contract.getHashEvidences(1);
      assert.equal(actualHashes.length, 0);

      const hashesOracle = [
        '8ddc2fec538e37d0c3934827827e0aaeeac27fbd8b0783d0ac8862b1bdaade44',
        '5c164752fffa950f2c038e0c8ce4ecc8e45195c8a984c78108fd07594178ecbc',
      ];
      await this.contract.uploadHashEvidence(1, toBytes64Array(hashesOracle), {
        from: oracleAccount,
      });
      const finalHashes = toStringArray(
        await this.contract.getHashEvidences(1)
      );
      assert.notStrictEqual(hashesOracle, finalHashes);
    });

    it('Upload evidences to existent activity being SE', async function () {
      await this.contractAdmin.createProject(1, seAccount, 'Test Project');
      await this.contractAdmin.createMilestone(1, 1, 1000, 'Make a better world');
      await this.contract.createActivity(
        1,
        1,
        1,
        oracleAccount,
        'Walk on moon'
      );
      const actualHashes = await this.contract.getHashEvidences(1);
      assert.equal(actualHashes.length, 0);

      const hashesSE = [
        '0dfc2fec538e37d0c3936827827e0aeeeac27fbd8b0783d0ac8862b1baaade44',
      ];
      await this.contract.uploadHashEvidence(1, toBytes64Array(hashesSE), {
        from: seAccount,
      });
      const finalHashes = toStringArray(
        await this.contract.getHashEvidences(1)
      );
      assert.notStrictEqual(hashesSE.concat(hashesSE), finalHashes);
    });

    it('Error - Upload evidences to existent activity being no oracle or SE', async function () {
      await this.contract.createActivity(
        1,
        1,
        1,
        oracleAccount,
        'Walk on moon'
      );
      const actualHashes = await this.contract.getHashEvidences(1);
      assert.equal(actualHashes.length, 0);

      const hashes = [
        '8ddc2fec538e37d0c3934827827e0aaeeac27fbd8b0783d0ac8862b1bdaade44',
      ];

      const bytes64hashes = toBytes64Array(hashes);

      await this.contract.uploadHashEvidence(1, bytes64hashes, {
        from: owner,
      }).should.be.rejectedWith('Invalid Sender');
    });
  });
});
