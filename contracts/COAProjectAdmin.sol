/**
 * AGPL License
 * Circle of Angels aims to democratize social impact financing.
 * It facilitate the investment process by utilizing smart contracts to develop impact milestones agreed upon by funders and the social entrepenuers.
 *
 * Copyright (C) 2019 AtixLabs, S.R.L <https://www.atixlabs.com>
 */

pragma solidity 0.5.0;

import "./COAOracle.sol";
import "openzeppelin-solidity/contracts/math/SafeMath.sol";
import "openzeppelin-solidity/contracts/math/Math.sol";
import "openzeppelin-solidity/contracts/access/roles/WhitelistedRole.sol";

contract COAProjectAdmin is WhitelistedRole {
    using SafeMath for uint256;
    using Math for uint256;

    // address init by constructor, could be changed for testing purpouses
    address oracleSCAddress;
    
    struct ActivityState {
        uint256 id;
        // 0: to verified, 1: verified
        uint256 state;
    }

    struct Milestone {
        uint256 id;
        uint256 budget;
        uint256[] activitiesIds;
        mapping (uint256 => ActivityState) activities;
        // 0: pending, 1: completed
        uint256 state;
        // 1: claimable, 2: claimed, 3: funded, 4: blocked
        uint256 budgetStatus;
        uint256 order;
        string description;
    }
    
    struct Project {
        uint256 id;
        address se;
        // 0: incompleted, 1: completed
        uint256 state;
        uint256[] milestonesIds;
        mapping (uint256 => Milestone) milestones;
        bool started;
        string name;
    }

    event NewProject(uint256 id);
    event NewMilestone(uint256 id, uint256 projectId, uint256 budget, string description, uint256 order);
    event NewActivity(uint256 id, uint256 milestoneId, uint256 projectId, string description);
    event ProjectStarted(uint256 id);
    // ===================== VALIDATIONS ======================================
    event ActivityValidated(uint256 id, uint256 milestoneId, uint256 projectId);
    event MilestoneCompleted(uint256 id, uint256 projectId);
    event ProjectCompleted(uint256 id);

    // =============== MILESTONE BUDGET STATUS CHANGE EVENTS ========================
    event MilestoneClaimable(uint256 id);
    event MilestoneClaimed(uint256 id);
    event MilestoneFunded(uint256 id);
    mapping (uint256 => Project) projects;

    address private admin;

    constructor(address initialAdmin, address oracleSC) WhitelistAdminRole() public {
        admin = initialAdmin;
        oracleSCAddress = oracleSC;
    }

    // =============== PROJECT ===================

    function createProject(uint256 aId, address aSe, string memory aName) public withNoExistingProject(aId) onlyWhitelisted() {
        projects[aId] = Project({
          id: aId,
          se: aSe,
          state: 0,
          milestonesIds: new uint256[](0),
          started: false,
          name: aName});
        emit NewProject(aId);
    }

    function getProject(uint256 id) public view returns (uint256) {
        return projects[id].id;
    }

    function getSe(uint256 projectId) public view returns (address) {
        return projects[projectId].se;
    }

    function getName(uint256 projectId) public view returns (string memory name) {
        return projects[projectId].name;
    }

    function getMilestone(uint256 projectId, uint256 milestoneId) public view returns (string memory desc) {
        return projects[projectId].milestones[milestoneId].description;
    }

    function startProject(uint256 aId) public withProjectNotStarted(aId) withValidateProject (aId) {
        projects[aId].started = true;
        emit ProjectStarted(aId);
        setFirstMilestoneClaimable(aId);
    }

    function isProjectCompleted(uint256 id) public view returns (bool) {
      return projects[id].state == 1;
    }

    // =============== MILESTONE BUDGET STATUS CHANGE ========================

    // Will be called while the milestones are being completed
    function setMilestoneClaimable(uint256 id, uint256 projectId) internal {
        projects[projectId].milestones[id].budgetStatus = 1;
        emit MilestoneClaimable(id);
    }
    
    function setFirstMilestoneClaimable(uint256 projectId) internal {
        setMilestoneClaimable(projects[projectId].milestonesIds[0], projectId);
    }
    
    function isMilestoneClaimable(uint256 id, uint256 projectId) internal view returns (bool) {
        return getMilestoneBudgetStatus(projectId, id) == 1;
    }

    function claimMilestone(uint256 id, uint256 projectId) public {
        // check milestone is claimable
        Milestone storage milestone = projects[projectId].milestones[id];
        require(milestone.budgetStatus == 1, "Milestone is not claimable");
        require(msg.sender == projects[projectId].se, "Milestones can only be claimed by project's SE");
        milestone.budgetStatus = 2;
        emit MilestoneClaimed(id);
    }

    function setMilestoneFunded(uint256 id, uint256 projectId) public {
        // check if the milestone is claimed 
        require(isMilestoneClaimed(id, projectId), "Milestone was not claimed yet");
        projects[projectId].milestones[id].budgetStatus = 3;
        emit MilestoneFunded(id);
        checkNextMilestonesClaimableFrom(id, projectId);
        checkProjectCompleted(projectId);
    }

    function checkNextMilestonesClaimableFrom(uint256 id, uint256 projectId) internal {
        uint256[] memory _milestoneIds = projects[projectId].milestonesIds;
        uint256 position = projects[projectId].milestones[id].order;
        for (uint i = position; i < _milestoneIds.length; i++) {
            checkMilestoneClaimable(_milestoneIds[i], projectId);
        }
    }

    function isMilestoneClaimed(uint256 id, uint256 projectId) internal view returns (bool) {
        return getMilestoneBudgetStatus(projectId, id) == 2;
    }

    function getNextMilestoneId(uint256 id, uint256 projectId) internal view returns (uint256) {
        return projects[projectId].milestonesIds[projects[projectId].milestones[id].order + 1];
    }

    // ============= MILESTONES ===================
    
    function createMilestone(uint256 milestoneId, uint256 projectId, uint256 aBudget, string memory aDescription) public 
        withProjectNotStarted(projectId) withNoExistingMilestone(milestoneId, projectId) onlyWhitelisted() {
        emit NewMilestone(milestoneId, projectId, aBudget, aDescription, projects[projectId].milestonesIds.length);
        projects[projectId].milestones[milestoneId] = Milestone({
          id: milestoneId,
          budget: aBudget,
          activitiesIds: new uint256[](0),
          state: 0,
          budgetStatus: 4,
          order: projects[projectId].milestonesIds.length,
          description: aDescription});

        projects[projectId].milestonesIds.push(milestoneId);
    }

    // ============= ACTIVITY ===================
    
    function createActivity(uint256 activityId, uint256 milestoneId, uint256 projectId, address aOracle,
        string memory aDescription) public withProjectNotStarted(projectId) withNoExistingActivity(activityId, milestoneId, projectId)
        onlyWhitelisted() {
        // send createActivity to the SC ORACLE
        COAOracle oracleSC = COAOracle(oracleSCAddress);
        oracleSC.createActivity(activityId, milestoneId, projectId, aOracle, aDescription);

        projects[projectId].milestones[milestoneId].activities[activityId] = ActivityState({
          id: activityId,
          state: 0
        });

        projects[projectId].milestones[milestoneId].activitiesIds.push(activityId);

        emit NewActivity(activityId, milestoneId, projectId, aDescription);
    }
    
    // called from the SC ORACLE
    function validateActivity(uint256 projectId, uint256 milestoneId, uint256 activityId) public {
        require(msg.sender == oracleSCAddress, "Not called from Oracle Contract");
        projects[projectId].milestones[milestoneId].activities[activityId].state = 1;
        emit ActivityValidated(projectId, milestoneId, activityId);
        checkMilestoneCompleted(projectId, milestoneId);
    }

    /**
      Checks that all activities from a milestone are validated,
      if they are, it marks the milestone as completed
    **/
    function checkMilestoneCompleted(uint256 projectId, uint256 milestoneId) internal {
        uint256 cantActivityValidated = 0;
        uint256[] memory _activityIds = projects[projectId].milestones[milestoneId].activitiesIds;

        // count activities validated
        for (uint i = 0; i < _activityIds.length; i++) {
          if (getActivityState(projectId, milestoneId, _activityIds[i]) == 1) {
              cantActivityValidated++;
          }
        }
        // mark milestone as completed
        if (cantActivityValidated == projects[projectId].milestones[milestoneId].activitiesIds.length) {
          projects[projectId].milestones[milestoneId].state = 1;
          emit MilestoneCompleted(milestoneId, projectId);
          checkNextMilestonesClaimableFrom(milestoneId, projectId);
          checkProjectCompleted(projectId);
        }
    }

    /**
      Checks that all milestones from a project are validated,
      if they are, it marks the project as completed
    **/  
    function checkProjectCompleted(uint256 projectId) internal {
        uint256 cantMilestoneValidated = 0;
        uint256[] memory _milestoneIds = projects[projectId].milestonesIds;

        // count milestones completed and funded
        for (uint i = 0; i < _milestoneIds.length; i++) {
          if (getMilestoneState(projectId, _milestoneIds[i]) == 1 &&
              getMilestoneBudgetStatus(projectId, _milestoneIds[i]) == 3) {
              cantMilestoneValidated++;
          }
        }
        // mark project as completed
        if (cantMilestoneValidated == projects[projectId].milestonesIds.length) {
          projects[projectId].state = 1;
          emit ProjectCompleted(projectId);
        }
    }

    // marks the milestone as claimable if all the requirements are matched
    // this should be checked on every milestone status change
    /**
      id: milestone id
     */
    function checkMilestoneClaimable(uint256 id, uint256 projectId) internal {
        // previous milestone needs to be completed and funded
        uint256 order = projects[projectId].milestones[id].order;
        uint256 budgetStatus = projects[projectId].milestones[id].budgetStatus;
        // doesn't need to check for the first milestone of the project
        // just check for blocked milestone, once claimable, claimed or funded it has no porpouse
        if (order > 0 && budgetStatus == 4 ) {
            uint256 previousOrder = projects[projectId].milestones[id].order.sub(1);
            uint256 previousId = projects[projectId].milestonesIds[previousOrder];
            if (projects[projectId].milestones[previousId].state == 1 && projects[projectId].milestones[previousId].budgetStatus == 3) {
                 setMilestoneClaimable(id, projectId);
            }
        }
    }
    
    // =========================== Modifiers =============================
    modifier withProjectNotStarted(uint256 id) {
      require(!projects[id].started, "Project already started");
      _;
    }

    modifier withValidateProject(uint256 id) {
      require(projects[id].milestonesIds.length != 0, "Project doesn't have milestones");
      uint256[] memory _milestoneIds = projects[id].milestonesIds;
      // Checks if a milestone doesn't have a activity
      for (uint i = 0; i < _milestoneIds.length; i++) {
        uint256 milestoneId = _milestoneIds[i];
        require(projects[id].milestones[milestoneId].activitiesIds.length != 0, "Milestone doesn't have activities");
      }
      _;
    }

    modifier withNoExistingActivity(uint256 activivityId, uint256 milestoneId, uint256 projectId) {
        require(projects[projectId].milestones[milestoneId].activities[activivityId].id == 0, "Activity already existed");
        _;
    }

    modifier withNoExistingMilestone(uint256 milestoneId, uint256 projectId) {
        require(projects[projectId].milestones[milestoneId].id == 0, "Milestone already existed");
        _;
    }

    modifier withNoExistingProject(uint256 projectId) {
        require(projects[projectId].id == 0, "Project already existed");
        _;
    }

    // ============= TESTING PORPOUSES ===================

    function getActivityState(uint256 projectId, uint256 milestoneId, uint256 activityId) public view returns (uint256) {
        return projects[projectId].milestones[milestoneId].activities[activityId].state;
    }

    function getMilestoneState(uint256 projectId, uint256 milestoneId) public view returns (uint256) {
        return projects[projectId].milestones[milestoneId].state;
    }

    function getMilestoneBudgetStatus(uint256 projectId, uint256 milestoneId) public view returns (uint256) {
      return projects[projectId].milestones[milestoneId].budgetStatus;
    }

    function getProjectState(uint256 projectId) public view returns (uint256) {
        return projects[projectId].state;
    }

    function setAdminSCAddress(address anAddress) public {
        oracleSCAddress = anAddress;
    }
} 
