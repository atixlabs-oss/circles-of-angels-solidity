/**
 * AGPL License
 * Circle of Angels aims to democratize social impact financing.
 * It facilitate the investment process by utilizing smart contracts to develop impact milestones agreed upon by funders and the social entrepenuers.
 *
 * Copyright (C) 2019 AtixLabs, S.R.L <https://www.atixlabs.com>
 */

pragma solidity 0.5.0;
import "./COAProjectAdmin.sol";
import "openzeppelin-solidity/contracts/ownership/Ownable.sol";

contract COAOracle is Ownable {

    // address init by setter
    address adminSCAddress;

    // 0: to verified, 1: verified
    struct Activity {
        uint256 projectId;
        uint256 milestoneId;
        uint256 id;
        address oracle;
        uint256 state;
        string description;
        byte[64][] evidences;
    }

    mapping (uint256 => Activity) activities;
    address private admin;
    
    event NewActivity(uint256 id, uint256 milestoneId, uint256 projectId, string description);

    constructor() public Ownable() {
    }

    function setAdminSCAddress(address anAddress) public onlyOwner() {
        adminSCAddress = anAddress;
    }

    function getAdminSCAddress() public view returns (address) {
        return adminSCAddress;
    }

    // ============== ACTIVITY ===================

    function createActivity(uint256 activityId, uint256 milestoneId, uint256 projectId, address aOracle,
     string memory aDescription) public withNoExistingActivity(activityId) onlySCAdmin() {
        activities[activityId] = Activity({
          id: activityId,
          milestoneId: milestoneId,
          projectId: projectId,
          oracle: aOracle,
          state: 0,
          description: aDescription,
          evidences: new byte[64][](0)});
        emit NewActivity(activityId, milestoneId, projectId, aDescription);
    }

    function getActivity(uint256 activityId) public view returns (string memory desc) {
        return activities[activityId].description;
    }

    function getActivityState(uint256 activityId) public view returns (uint256) {
        return activities[activityId].state;
    }

    function getActivityDescription(uint256 activityId) public view returns (string memory desc) {
        return activities[activityId].description;
    }

    function getActivityOracle(uint256 activityId) public view returns (address) {
        return activities[activityId].oracle;
    }

    function validateActivity(uint256 activityId) public withExistingActivity(activityId) withNoValidatedActivity(activityId)  {
        require(msg.sender == activities[activityId].oracle, "Invalid oracle");
        activities[activityId].state = 1;

        // send the verification to the project admin contract
        COAProjectAdmin coaAdmin = COAProjectAdmin(adminSCAddress);
        coaAdmin.validateActivity(activities[activityId].projectId, activities[activityId].milestoneId, activityId);
    }

    function uploadHashEvidence(uint256 activityId, byte[64][] memory hashes) public {
        COAProjectAdmin coaAdmin = COAProjectAdmin(adminSCAddress);
        address se = coaAdmin.getSe(activities[activityId].projectId);
        require(msg.sender == activities[activityId].oracle || msg.sender == se, "Invalid Sender");

        for (uint i=0; i < hashes.length; i++) {
            activities[activityId].evidences.push(hashes[i]);
        }
    }

    function getHashEvidences(uint256 activityId) public view returns (byte[64][] memory){
        return activities[activityId].evidences;
    }

    // ============================= Modifiers ==================================

    modifier onlySCAdmin() {
        require(msg.sender == adminSCAddress, "Not called from SC admin");
        _;
    }

    modifier withNoExistingActivity(uint256 aId) {
        require(activities[aId].id == 0, "Activity already existed");
        _;
    }

     modifier withNoValidatedActivity(uint256 aId) {
        require(activities[aId].state != 1, "Activity already validated");
        _;
    }

    modifier withExistingActivity(uint256 aId) {
        require(activities[aId].id != 0, "Activity doesn't exist");
        _;
    }
}
